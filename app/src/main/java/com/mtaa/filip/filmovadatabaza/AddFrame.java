package com.mtaa.filip.filmovadatabaza;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddFrame extends AppCompatActivity {

    private RequestQueue queue;
    private ProgressDialog ProgressDialog;
    private ArrayList<String> selectedGenresIDList;
    private ArrayList<String> genresIDList;
    private boolean loaded;
    private boolean connected;
    private JSONObject obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle extras = getIntent().getExtras();
        connected = true;

        if(extras.getString("data").equals("Movies")) {
            setContentView(R.layout.activity_add_movie);
        }
        if(extras.getString("data").equals("Series")) {
            setContentView(R.layout.activity_add_series);
        }
        if(extras.getString("data").equals("Actors") || extras.getString("data").equals("Directors")) {
            setContentView(R.layout.activity_add_people);
            TextView people = (TextView) findViewById(R.id.textAddPeople);
            String add;
            if(extras.getString("data").equals("Actors")) {
                add = "Add an actor";
            } else {
                add = "Add a director";
            }
            people.setText(add);
        }

        if(extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
            genresIDList = extras.getStringArrayList("genres");

            final MultiSpinner spinner = (MultiSpinner) findViewById(R.id.input1);
            ArrayList<String> spinnerList = extras.getStringArrayList("spinner");
            spinner.setItems(spinnerList);
            for (int i = 0; i < 5; i++) {
                spinner.setSelection(i, false);
            }
            spinner.setListener(new MultiSpinner.OnMultipleItemsSelectedListener() {
                @Override
                public void selectedIndices(List<Integer> indices) {
                    selectedGenresIDList = new ArrayList<String>();
                    for (int i = 0; i < indices.size(); i++) {
                        selectedGenresIDList.add(genresIDList.get(indices.get(i)));
                    }
                }

                @Override
                public void selectedStrings(List<String> strings) {

                }
            });
        }

        Button confirm = (Button) findViewById(R.id.confirm);
        assert confirm != null;
        confirm.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        loaded = false;
                        obj = new JSONObject();

                        final EditText nameT = (EditText) findViewById(R.id.editText);
                        if (nameT.getText().toString().matches("") == false) {
                            try {
                                obj.put("Name", nameT.getText().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
                                final EditText releaseT = (EditText) findViewById(R.id.editText2);
                                final EditText writerT = (EditText) findViewById(R.id.editText3);
                                final EditText countryT = (EditText) findViewById(R.id.editText4);
                                final EditText languageT = (EditText) findViewById(R.id.editText5);
                                final EditText lengthT = (EditText) findViewById(R.id.editText6);
                                final EditText pictureT = (EditText) findViewById(R.id.editText7);

                                if(selectedGenresIDList != null) {
                                    if(selectedGenresIDList.size() != 0) {
                                        try {
                                            JSONArray genresArray = new JSONArray();
                                            for(int i = 0; i < selectedGenresIDList.size(); i++) {
                                                JSONObject item = new JSONObject();
                                                item.put("___class", "Genres");
                                                item.put("objectId", selectedGenresIDList.get(i));
                                                genresArray.put(item);
                                            }
                                            obj.put("Genres", genresArray);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                try {
                                    if (releaseT.getText().toString().matches("") == false) {
                                        obj.put("ReleaseDate", releaseT.getText().toString());
                                    }
                                    if (writerT.getText().toString().matches("") == false) {
                                        obj.put("Writer", writerT.getText().toString());
                                    }
                                    if (countryT.getText().toString().matches("") == false) {
                                        obj.put("Country", countryT.getText().toString());
                                    }
                                    if (languageT.getText().toString().matches("") == false) {
                                        obj.put("Language", languageT.getText().toString());
                                    }
                                    if (lengthT.getText().toString().matches("") == false) {
                                        obj.put("Length", lengthT.getText().toString());
                                    }
                                    if (pictureT.getText().toString().matches("") == false) {
                                        obj.put("Picture", pictureT.getText().toString());
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                if (extras.getString("data").equals("Series")) {
                                    final EditText episodesT = (EditText) findViewById(R.id.editText9);
                                    if (episodesT.getText().toString().matches("") == false) {
                                        try {
                                            obj.put("Episodes", episodesT.getText().toString());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            } else {
                                final EditText cityT = (EditText) findViewById(R.id.editText2);
                                final EditText bornT = (EditText) findViewById(R.id.editText3);
                                final EditText ageT = (EditText) findViewById(R.id.editText4);
                                final EditText pictureT = (EditText) findViewById(R.id.editText5);

                                try {
                                    if (cityT.getText().toString().matches("") == false) {
                                        obj.put("City", cityT.getText().toString());
                                    }
                                    if (bornT.getText().toString().matches("") == false) {
                                        obj.put("Born", bornT.getText().toString());
                                    }
                                    if (ageT.getText().toString().matches("") == false) {
                                        obj.put("Age", ageT.getText().toString());
                                    }
                                    if (pictureT.getText().toString().matches("") == false) {
                                        obj.put("Picture", pictureT.getText().toString());
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        new ProgressTask().execute();
                    }
                }
        );
    }

    public class ProgressTask extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute(){
            ProgressDialog = new ProgressDialog(AddFrame.this);
            ProgressDialog.setIndeterminate(false);
            ProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            ProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            synchronized (this) {

                queue = Volley.newRequestQueue(AddFrame.this);
                final Bundle extras = getIntent().getExtras();

                String url = "https://api.backendless.com/v1/data/" + extras.getString("data");

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        loaded = true;
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                        headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                        headers.put("Content-Type", "application/json");
                        headers.put("application-type", " REST");
                        return headers;
                    }
                };

                queue.add(request);

                final AlertDialog.Builder builder = new AlertDialog.Builder(AddFrame.this);
                builder.setTitle("No connection");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(AddFrame.this, MainFrame.class));
                    }
                });

                builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                int missed = 0;
                while(loaded != true) {
                    try {
                        this.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(missed == 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                        connected = false;
                        break;
                    } else missed++;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ProgressDialog.dismiss();
            if(connected == false) {
                return;
            }

            final Bundle extras = getIntent().getExtras();

            if(loaded == true) {
                if(extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
                    Intent i = new Intent(AddFrame.this, TableFrame.class);
                    i.putExtra("data", extras.getString("data"));
                    startActivity(i);
                } else {
                    Intent i = new Intent(AddFrame.this, PeopleTableFrame.class);
                    i.putExtra("data", "People");
                    startActivity(i);
                }
            }
        }
    }
}
