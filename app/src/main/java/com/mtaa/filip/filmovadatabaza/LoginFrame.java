package com.mtaa.filip.filmovadatabaza;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.BackendlessCallback;

public class LoginFrame extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_frame);

        Button button = (Button)findViewById(R.id.login);
        final EditText username = (EditText)findViewById(R.id.username);
        final EditText password = (EditText)findViewById(R.id.password);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Wrong username or password");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
                System.exit(0);
            }
        });

        assert button != null;
        button.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        String user = username.getText().toString();
                        String pass = password.getText().toString();
                        if((user.equals("erik") && pass.equals("erik")) || (user.equals("filip") && pass.equals("filip"))) {
                            //dalsie okno
                            startActivity(new Intent(LoginFrame.this,MainFrame.class));
                        } else
                        {
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    }
                }
        );
    }
}