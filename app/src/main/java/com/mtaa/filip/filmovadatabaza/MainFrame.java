package com.mtaa.filip.filmovadatabaza;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainFrame extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_frame);
        Button movies = (Button)findViewById(R.id.movies);
        Button series = (Button)findViewById(R.id.series);
        Button people = (Button)findViewById(R.id.people);
        Button logout = (Button)findViewById(R.id.logout);

        assert movies != null;
        movies.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent(MainFrame.this, TableFrame.class);
                        //premenne medzi aktivitami
                        i.putExtra("data", "Movies");
                        startActivity(i);
                    }
                }
        );

        assert series != null;
        series.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent(MainFrame.this, TableFrame.class);
                        i.putExtra("data", "Series");
                        startActivity(i);
                    }
                }
        );

        assert people != null;
        people.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent(MainFrame.this, PeopleTableFrame.class);
                        i.putExtra("data", "People");
                        startActivity(i);
                    }
                }
        );

        assert logout != null;
        logout.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        startActivity(new Intent(MainFrame.this, LoginFrame.class));
                    }
                }
        );
    }
}