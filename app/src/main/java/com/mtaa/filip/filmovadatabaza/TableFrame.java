package com.mtaa.filip.filmovadatabaza;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableFrame extends AppCompatActivity {

    private RequestQueue queue;
    private ProgressDialog ProgressDialog;
    private ArrayList<String> list = new ArrayList<String>();
    private ArrayList<String> listID = new ArrayList<String>();
    private ArrayList<ArrayList<String>> listGenres = new ArrayList<ArrayList<String>>();
    private ArrayList<String> selectedList = new ArrayList<String>();
    private ArrayList<String> selectedListID = new ArrayList<String>();
    private ArrayList<String> spinnerList = new ArrayList<String>();
    private ArrayList<String> genresList = new ArrayList<String>();
    private ArrayList<String> toDelete;
    private ArrayList<ListItem> itemList;
    private int deleted;
    private int loadedCount;
    private boolean loaded;
    private boolean selected;
    private boolean connected;
    private int obs;
    private int pages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle extras = getIntent().getExtras();
        connected = true;

        if(extras.getString("data").equals("Movies")) {
            setContentView(R.layout.activity_movies_frame);

            Button add = (Button) findViewById(R.id.addMovies);
            assert add != null;
            add.setOnClickListener(
                    new Button.OnClickListener() {
                        public void onClick(View v) {
                            Intent i = new Intent(TableFrame.this, AddFrame.class);
                            i.putExtra("data", extras.getString("data"));
                            i.putExtra("genres", genresList);
                            i.putExtra("spinner", spinnerList);
                            startActivity(i);
                        }
                    }
            );
        }

        if(extras.getString("data").equals("Series")) {
            setContentView(R.layout.activity_series_frame);

            Button add = (Button)findViewById(R.id.add);
            assert add != null;
            add.setOnClickListener(
                    new Button.OnClickListener() {
                        public void onClick(View v) {
                            Intent i = new Intent(TableFrame.this, AddFrame.class);
                            i.putExtra("data", extras.getString("data"));
                            i.putExtra("genres", genresList);
                            i.putExtra("spinner", spinnerList);
                            startActivity(i);
                        }
                    }
            );
        }

        Button delete = (Button) findViewById(R.id.delete);
        assert delete != null;
        delete.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        if(extras.getString("data").equals("Movies")) {
                            setContentView(R.layout.activity_movies_frame_delete);
                        }
                        if(extras.getString("data").equals("Series")) {
                            setContentView(R.layout.activity_series_frame_delete);
                        }

                        displayListView();

                        Button confirm = (Button) findViewById(R.id.confirm);
                        assert confirm != null;
                        confirm.setOnClickListener(
                                new Button.OnClickListener() {
                                    public void onClick(View v) {
                                        toDelete = new ArrayList<String>();
                                        for(int i = 0; i < itemList.size(); i++) {
                                            if(itemList.get(i).isSelected()) {
                                                toDelete.add(listID.get(i));
                                            }
                                        }
                                        new ProgressTaskDelete().execute();
                                    }
                                }
                        );
                    }
                }
        );

        //stahovanie dat bezi v pozadi pomocou AsyncTask, zatial sa ukazuje loading
        new ProgressTask().execute();
    }

    private void displayListView() {
        itemList = new ArrayList<ListItem>();
        for(int i = 0; i < list.size(); i++) {
            ListItem item = new ListItem(list.get(i),false);
            itemList.add(item);
        }

        CustomAdapter dataAdapter = new CustomAdapter(this, R.layout.listview_checkbox, itemList);
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(dataAdapter);
    }

    public class ProgressTask extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute(){
            ProgressDialog = new ProgressDialog(TableFrame.this);
            ProgressDialog.setIndeterminate(false);
            ProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            ProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            loadedCount = 0;
            loaded = false;
            pages = -1;

            synchronized (this) {

                queue = Volley.newRequestQueue(TableFrame.this);
                final Bundle extras = getIntent().getExtras();

                //---------------------GET data
                //url podla vyberu v predoslej aktivite
                String url = "https://api.backendless.com/v1/data/" + extras.getString("data");

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //get prijme najviac len 10 objektov na stranu
                            String objects = response.getString("totalObjects");
                            obs = Integer.parseInt(objects);
                            if(obs % 10 == 0) {
                                pages = obs / 10;
                            }
                            else pages = (obs / 10) + 1;

                            JSONArray array = response.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                list.add(array.getJSONObject(i).getString("Name"));
                                listID.add(array.getJSONObject(i).getString("objectId"));

                                JSONArray genresArray = array.getJSONObject(i).getJSONArray("Genres");
                                ArrayList<String> genres = new ArrayList<String>();
                                for(int j = 0; j < genresArray.length(); j++) {
                                    genres.add(genresArray.getJSONObject(j).getString("Genre"));
                                }
                                listGenres.add(genres);
                            }
                            loadedCount++;
                            if (pages > 1) {
                                for (int i = 0; i < pages - 1; i++) {
                                    //automaticky sa posiela aj adresa s ofsetom
                                    String url = response.getString("nextPage");
                                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                JSONArray array = response.getJSONArray("data");
                                                for (int i = 0; i < array.length(); i++) {
                                                    list.add(array.getJSONObject(i).getString("Name"));
                                                    listID.add(array.getJSONObject(i).getString("objectId"));

                                                    JSONArray genresArray = array.getJSONObject(i).getJSONArray("Genres");
                                                    ArrayList<String> genres = new ArrayList<String>();
                                                    for(int j = 0; j < genresArray.length(); j++) {
                                                        genres.add(genresArray.getJSONObject(j).getString("Genre"));
                                                    }
                                                    listGenres.add(genres);
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            loadedCount++;
                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                        }
                                    }) {
                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                            HashMap<String, String> headers = new HashMap<String, String>();
                                            headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                                            headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                                            headers.put("application-type", " REST");
                                            return headers;
                                        }
                                    };

                                    queue.add(request);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                        headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                        headers.put("application-type", " REST");
                        return headers;
                    }
                };

                //-----------GET genresList
                String url2 = "https://api.backendless.com/v1/data/Genres";
                JsonObjectRequest request2 = new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray array = response.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                genresList.add(array.getJSONObject(i).getString("objectId"));
                                spinnerList.add(array.getJSONObject(i).getString("Genre"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        loaded = true;
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                        headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                        headers.put("application-type", " REST");
                        return headers;
                    }
                };

                queue.add(request);

                //ak pridlho caka, chyba so spojenim
                final AlertDialog.Builder builder = new AlertDialog.Builder(TableFrame.this);
                builder.setTitle("No connection");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(TableFrame.this, MainFrame.class));
                    }
                });

                builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                //caka pokym sa nespracuje response
                int missed = 0;
                while(loadedCount != pages) {
                    try {
                        this.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(missed == 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                        connected = false;
                        break;
                    } else missed++;
                }

                queue.add(request2);

                missed = 0;
                while(loaded != true) {
                    try {
                        this.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(missed == 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                        connected = false;
                        break;
                    } else missed++;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ProgressDialog.dismiss();
            if(connected == false) {
                return;
            }

            final Bundle extras = getIntent().getExtras();
            selected = false;

            //naladovanie dat do listu
            ArrayAdapter adapter = new ArrayAdapter<String>(TableFrame.this, R.layout.listview, list);
            ListView listView = (ListView) findViewById(R.id.listview);
            assert listView != null;
            listView.setAdapter(adapter);

            final MultiSpinner spinner = (MultiSpinner) findViewById(R.id.input1);
            spinner.setItems(spinnerList);
            for(int i = 0; i < 5; i++) {
                spinner.setSelection(i,true);
            }
            spinner.setListener(new MultiSpinner.OnMultipleItemsSelectedListener() {
                @Override
                public void selectedIndices(List<Integer> indices) {

                }

                @Override
                public void selectedStrings(List<String> strings) {
                    selected = true;
                    selectedList.clear();
                    selectedListID.clear();
                    for (int i = 0; i < list.size(); i++) {
                        for (int j = 0; j < listGenres.get(i).size(); j++) {
                            boolean match = false;
                            for (int k = 0; k < spinner.getSelectedStrings().size(); k++) {
                                if (spinner.getSelectedStrings().get(k).equals(listGenres.get(i).get(j))) {
                                    selectedList.add(list.get(i));
                                    selectedListID.add(listID.get(i));
                                    match = true;
                                    break;
                                }
                            }
                            if (match) {
                                break;
                            }
                        }
                    }
                    ArrayAdapter adapter = new ArrayAdapter<String>(TableFrame.this, R.layout.listview, selectedList);
                    ListView listView = (ListView) findViewById(R.id.listview);
                    listView.setAdapter(adapter);
                }
            });

            assert listView != null;
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String chosenID;
                    if(selected == false) {
                        chosenID = listID.get(position);
                    } else {
                        chosenID = selectedListID.get(position);
                    }
                    Intent i = new Intent(TableFrame.this, ItemFrame.class);
                    i.putExtra("chosenID", chosenID);
                    i.putExtra("data", extras.getString("data"));
                    startActivity(i);
                }
            });

        }
    }

    public class ProgressTaskDelete extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute(){
            ProgressDialog = new ProgressDialog(TableFrame.this);
            ProgressDialog.setIndeterminate(false);
            ProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            ProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            synchronized (this) {

                queue = Volley.newRequestQueue(TableFrame.this);
                deleted = 0;
                final Bundle extras = getIntent().getExtras();

                for(int i = 0; i < toDelete.size(); i++) {
                    String url = "https://api.backendless.com/v1/data/" + extras.getString("data") + "/" + toDelete.get(i);

                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            deleted++;
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                            headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                            //headers.put("Content-Type", "application/json");
                            headers.put("application-type", " REST");
                            return headers;
                        }
                    };

                    queue.add(request);
                }

                //ak pridlho caka, chyba so spojenim
                final AlertDialog.Builder builder = new AlertDialog.Builder(TableFrame.this);
                builder.setTitle("No connection");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(TableFrame.this, MainFrame.class));
                    }
                });

                builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                int missed = 0;
                while(deleted != toDelete.size()) {
                    try {
                        this.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(missed == 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                        connected = false;
                        break;
                    } else missed++;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ProgressDialog.dismiss();
            if(connected == false) {
                return;
            }

            final Bundle extras = getIntent().getExtras();

            if(extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
                Intent i = new Intent(TableFrame.this, TableFrame.class);
                i.putExtra("data", extras.getString("data"));
                startActivity(i);
            }
        }
    }
}