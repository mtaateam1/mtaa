package com.mtaa.filip.filmovadatabaza;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PeopleTableFrame extends AppCompatActivity {

    private RequestQueue queue;
    private android.app.ProgressDialog ProgressDialog;
    private ArrayList<String> list = new ArrayList<String>();
    private ArrayList<String> listID = new ArrayList<String>();
    private ArrayList<String> entity = new ArrayList<String>();
    private ArrayList<ListItem> itemList;
    private ArrayList<String> toDelete;
    private ArrayList<String> entityToDelete;
    private int deleted;
    private int loadedCount;
    private int loadedCount2;
    private int obs;
    private int obs2;
    private int pages;
    private int pages2;
    private boolean connected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people_frame);
        connected = true;
        final Bundle extras = getIntent().getExtras();

        Button add = (Button) findViewById(R.id.add);
        assert add != null;
        add.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {

                        final AlertDialog.Builder builder = new AlertDialog.Builder(PeopleTableFrame.this);
                        builder.setTitle("Add to?");
                        builder.setPositiveButton("Actors", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(PeopleTableFrame.this, AddFrame.class);
                                i.putExtra("data", "Actors");
                                startActivity(i);
                            }
                        });

                        builder.setNegativeButton("Directors", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(PeopleTableFrame.this, AddFrame.class);
                                i.putExtra("data", "Directors");
                                startActivity(i);
                            }
                        });

                        runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                    }
                }
        );

        Button delete = (Button) findViewById(R.id.delete);
        assert delete != null;
        delete.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        setContentView(R.layout.activity_people_frame_delete);

                        displayListView();

                        Button confirm = (Button) findViewById(R.id.confirm);
                        assert confirm != null;
                        confirm.setOnClickListener(
                                new Button.OnClickListener() {
                                    public void onClick(View v) {
                                        toDelete = new ArrayList<String>();
                                        entityToDelete = new ArrayList<String>();
                                        for (int i = 0; i < itemList.size(); i++) {
                                            if (itemList.get(i).isSelected()) {
                                                toDelete.add(listID.get(i));
                                                entityToDelete.add(entity.get(i));
                                            }
                                        }
                                        new ProgressTaskDelete().execute();
                                    }
                                }
                        );
                    }
                }
        );

        new ProgressTask().execute();
    }

    private void displayListView() {
        itemList = new ArrayList<ListItem>();
        for(int i = 0; i < list.size(); i++) {
            ListItem item = new ListItem(list.get(i),false);
            itemList.add(item);
        }

        CustomAdapter dataAdapter = new CustomAdapter(this, R.layout.listview_checkbox, itemList);
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(dataAdapter);
    }

    public class ProgressTask extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute(){
            ProgressDialog = new ProgressDialog(PeopleTableFrame.this);
            ProgressDialog.setIndeterminate(false);
            ProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            ProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            loadedCount = 0;
            loadedCount2 = 0;
            pages = -1;
            pages2 = -1;

            synchronized (this) {

                queue = Volley.newRequestQueue(PeopleTableFrame.this);

                //--------------Actors
                String url = "https://api.backendless.com/v1/data/Actors";

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String objects = response.getString("totalObjects");
                            obs = Integer.parseInt(objects);
                            if(obs % 10 == 0) {
                                pages = obs / 10;
                            }
                            else pages = (obs / 10) + 1;

                            JSONArray array = response.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                list.add(array.getJSONObject(i).getString("Name"));
                                listID.add(array.getJSONObject(i).getString("objectId"));
                                entity.add("Actors");
                            }
                            loadedCount++;
                            if (pages > 1) {
                                for (int i = 0; i < pages - 1; i++) {
                                    String url = response.getString("nextPage");
                                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                JSONArray array = response.getJSONArray("data");
                                                for (int i = 0; i < array.length(); i++) {
                                                    list.add(array.getJSONObject(i).getString("Name"));
                                                    listID.add(array.getJSONObject(i).getString("objectId"));
                                                    entity.add("Actors");
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            loadedCount++;
                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                        }
                                    }) {
                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                            HashMap<String, String> headers = new HashMap<String, String>();
                                            headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                                            headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                                            headers.put("application-type", " REST");
                                            return headers;
                                        }
                                    };

                                    queue.add(request);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                        headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                        headers.put("application-type", " REST");
                        return headers;
                    }
                };

                //--------------Directors
                String url2 = "https://api.backendless.com/v1/data/Directors";

                JsonObjectRequest request2 = new JsonObjectRequest(Request.Method.GET, url2, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String objects = response.getString("totalObjects");
                            obs2 = Integer.parseInt(objects);
                            if(obs2 % 10 == 0) {
                                pages2 = obs2 / 10;
                            }
                            else pages2 = (obs2 / 10) + 1;

                            JSONArray array = response.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                list.add(array.getJSONObject(i).getString("Name"));
                                listID.add(array.getJSONObject(i).getString("objectId"));
                                entity.add("Directors");
                            }
                            loadedCount2++;
                            if (pages2 > 1) {
                                for (int i = 0; i < pages2 - 1; i++) {
                                    String url = response.getString("nextPage");
                                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                                        @Override
                                        public void onResponse(JSONObject response) {
                                            try {
                                                JSONArray array = response.getJSONArray("data");
                                                for (int i = 0; i < array.length(); i++) {
                                                    list.add(array.getJSONObject(i).getString("Name"));
                                                    listID.add(array.getJSONObject(i).getString("objectId"));
                                                    entity.add("Directors");
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            loadedCount2++;
                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                        }
                                    }) {
                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                            HashMap<String, String> headers = new HashMap<String, String>();
                                            headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                                            headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                                            headers.put("application-type", " REST");
                                            return headers;
                                        }
                                    };

                                    queue.add(request);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                        headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                        headers.put("application-type", " REST");
                        return headers;
                    }
                };

                queue.add(request);

                final AlertDialog.Builder builder = new AlertDialog.Builder(PeopleTableFrame.this);
                builder.setTitle("No connection");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(PeopleTableFrame.this, MainFrame.class));
                    }
                });

                builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                int missed = 0;
                while(loadedCount != pages) {
                    try {
                        this.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(missed == 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                        connected = false;
                        break;
                    } else missed++;
                }

                queue.add(request2);

                missed = 0;
                while(loadedCount2 != pages2) {
                    try {
                        this.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(missed == 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                        connected = false;
                        break;
                    } else missed++;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ProgressDialog.dismiss();
            if(connected == false) {
                return;
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(PeopleTableFrame.this, R.layout.listview, list);
            ListView listView = (ListView) findViewById(R.id.listPeople);
            listView.setAdapter(adapter);

            assert listView != null;
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String chosenID = listID.get(position);
                    Intent i = new Intent(PeopleTableFrame.this, ItemFrame.class);
                    i.putExtra("chosenID", chosenID);
                    if(position < obs) {
                        i.putExtra("data", "Actors");
                    } else {
                        i.putExtra("data", "Directors");
                    }
                    startActivity(i);
                }
            });

        }
    }

    public class ProgressTaskDelete extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute(){
            ProgressDialog = new ProgressDialog(PeopleTableFrame.this);
            ProgressDialog.setIndeterminate(false);
            ProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            ProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            synchronized (this) {

                queue = Volley.newRequestQueue(PeopleTableFrame.this);
                deleted = 0;
                final Bundle extras = getIntent().getExtras();

                for(int i = 0; i < toDelete.size(); i++) {
                    String url = "https://api.backendless.com/v1/data/" + entityToDelete.get(i) + "/" + toDelete.get(i);

                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            deleted++;
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            HashMap<String, String> headers = new HashMap<String, String>();
                            headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                            headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                            //headers.put("Content-Type", "application/json");
                            headers.put("application-type", " REST");
                            return headers;
                        }
                    };

                    queue.add(request);
                }

                //ak pridlho caka, chyba so spojenim
                final AlertDialog.Builder builder = new AlertDialog.Builder(PeopleTableFrame.this);
                builder.setTitle("No connection");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(PeopleTableFrame.this, MainFrame.class));
                    }
                });

                builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                int missed = 0;
                while(deleted != toDelete.size()) {
                    try {
                        this.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(missed == 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                        connected = false;
                        break;
                    } else missed++;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ProgressDialog.dismiss();
            if(connected == false) {
                return;
            }

            final Bundle extras = getIntent().getExtras();

            Intent i = new Intent(PeopleTableFrame.this, PeopleTableFrame.class);
            i.putExtra("data", extras.getString("data"));
            startActivity(i);
        }
    }
}