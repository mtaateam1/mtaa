package com.mtaa.filip.filmovadatabaza;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ItemFrame extends AppCompatActivity {

    private RequestQueue queue;
    private ProgressDialog ProgressDialog;
    private ArrayList<String> itemList = new ArrayList<String>();
    private boolean loaded;
    private boolean connected;
    private String name;
    private String release;
    private String imgURL;
    private ProgressBar spinner;
    private Bitmap img;
    private boolean imgUpdated;
    private EditText writer;
    private EditText country;
    private EditText length;
    private EditText language;
    private EditText episodes;
    private EditText age;
    private EditText born;
    private EditText city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_frame);
        spinner = (ProgressBar) findViewById(R.id.spinner);
        spinner.setVisibility(View.GONE);
        connected = true;

        new ProgressTask().execute();
    }

    public class ProgressTask extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute(){
            ProgressDialog = new ProgressDialog(ItemFrame.this);
            ProgressDialog.setIndeterminate(false);
            ProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            ProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            synchronized (this) {

                queue = Volley.newRequestQueue(ItemFrame.this);
                loaded = false;
                final Bundle extras = getIntent().getExtras();

                String url = "https://api.backendless.com/v1/data/" + extras.getString("data") + "/" + extras.getString("chosenID");

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            name = response.getString("Name");
                            imgURL = response.getString("Picture");
                            if(extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
                                JSONArray genresArray = response.getJSONArray("Genres");
                                String genres = null;
                                if(genresArray.length() > 0) {
                                    genres = genresArray.getJSONObject(0).getString("Genre");
                                }
                                if(genresArray.length() > 1) {
                                    for(int i = 1; i < genresArray.length(); i++) {
                                        genres += ", " + genresArray.getJSONObject(i).getString("Genre");
                                    }
                                }

                                JSONArray actorsArray = response.getJSONArray("Actors");
                                String actors = null;
                                if(actorsArray.length() > 0) {
                                    actors = actorsArray.getJSONObject(0).getString("Name");
                                }
                                if(actorsArray.length() > 1) {
                                    for(int i = 1; i < actorsArray.length(); i++) {
                                        actors += ", " + actorsArray.getJSONObject(i).getString("Name");
                                    }
                                }

                                JSONArray directorsArray = response.getJSONArray("Director");
                                String directors = null;
                                if(directorsArray.length() > 0) {
                                    directors = directorsArray.getJSONObject(0).getString("Name");
                                }
                                if(directorsArray.length() > 1) {
                                    for(int i = 1; i < directorsArray.length(); i++) {
                                        directors += ", " + directorsArray.getJSONObject(i).getString("Name");
                                    }
                                }

                                release = response.getString("ReleaseDate");
                                itemList.add("Genre: " + genres);
                                itemList.add("Director: " + directors);
                                itemList.add("Writer: " + response.getString("Writer"));
                                itemList.add("Stars: " + actors);
                                itemList.add("Country: " + response.getString("Country"));
                                itemList.add("Length: " + response.getString("Length"));
                                itemList.add("Language: " + response.getString("Language"));
                                if(extras.getString("data").equals("Series")) {
                                    itemList.add("Episodes: " + response.getString("Episodes"));
                                }
                            } else {
                                itemList.add("City: " + response.getString("City"));
                                itemList.add("Born: " + response.getString("Born"));
                                itemList.add("Age: " + response.getString("Age"));
                            }
                    ////////Obrazok sa stiahne
                            new DownloadImageTask((ImageView) findViewById(R.id.image))
                                    .execute(imgURL);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            final ImageView image = (ImageView) findViewById(R.id.image);

                            image.setOnClickListener(
                                    new Button.OnClickListener() {
                                        public void onClick(View v) {
                                            Intent i = new Intent(ItemFrame.this, FullscreenFrame.class);
                                            i.putExtra("data",imgURL);
                                            startActivity(i);
                                        }
                                    }
                            );
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            loaded = true;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (NullPointerException npe) {
                            npe.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                        headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                        headers.put("application-type", " REST");
                        return headers;
                    }
                };

                queue.add(request);

                //ak pridlho caka, chyba so spojenim
                final AlertDialog.Builder builder = new AlertDialog.Builder(ItemFrame.this);
                builder.setTitle("No connection");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(ItemFrame.this, MainFrame.class));
                    }
                });

                builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                int missed = 0;
                while(loaded != true) {
                    try {
                        this.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(missed == 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                        connected = false;
                        break;
                    } else missed++;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ProgressDialog.dismiss();
            if(connected == false) {
                return;
            }

            final Bundle extras = getIntent().getExtras();

            TextView title = (TextView) findViewById(R.id.name);
            if(extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
                title.setText(name + " (" + release + ")");
            } else {
                title.setText(name);
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(ItemFrame.this, R.layout.listview, itemList);
            ListView listView = (ListView) findViewById(R.id.itemList);
            listView.setAdapter(adapter);

            Button edit = (Button) findViewById(R.id.edit);
            assert edit != null;
            edit.setOnClickListener(
                    new Button.OnClickListener() {
                        public void onClick(View v) {
                            System.out.println(extras.getString("data"));
                            if(extras.getString("data").equals("Movies")) {
                                setContentView(R.layout.activity_item_frame_edit_movie);
                            }
                            if(extras.getString("data").equals("Series")) {
                                setContentView(R.layout.activity_item_frame_edit_series);
                            }
                            if(extras.getString("data").equals("Actors") || extras.getString("data").equals("Directors")) {
                                setContentView(R.layout.activity_item_frame_edit_people);
                            }

                            spinner = (ProgressBar) findViewById(R.id.spinner);
                            spinner.setVisibility(View.GONE);

                            imgUpdated = false;

                            if(extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
                                //relacie sa nedaju zmenit
                                itemList.remove(0);
                                itemList.remove(0);
                                itemList.remove(1);
                            }

                            final ImageView image = (ImageView) findViewById(R.id.image);
                            image.setImageBitmap(img);
                            image.setOnClickListener(
                                    new Button.OnClickListener() {
                                        public void onClick(View v) {
                                            AlertDialog.Builder alert = new AlertDialog.Builder(ItemFrame.this);

                                            alert.setTitle("Set image URL");

                                            final EditText input = new EditText(ItemFrame.this);
                                            input.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                                            alert.setView(input);

                                            alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                   imgURL = input.getText().toString();
                                                    new DownloadImageTask(image)
                                                            .execute(imgURL);
                                                    imgUpdated = true;
                                                }
                                            });

                                            alert.show();
                                        }
                                    }
                            );

                            System.out.println(itemList);

                            if(extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
                                writer = (EditText) findViewById(R.id.writer);
                                country = (EditText) findViewById(R.id.country);
                                length = (EditText) findViewById(R.id.length);
                                language = (EditText) findViewById(R.id.language);

                                writer.setHint(itemList.get(0));
                                country.setHint(itemList.get(1));
                                length.setHint(itemList.get(2));
                                language.setHint(itemList.get(3));

                                if(extras.getString("data").equals("Series")) {
                                    episodes = (EditText) findViewById(R.id.episodes);
                                    episodes.setHint(itemList.get(4));
                                }
                            } else {
                                age = (EditText) findViewById(R.id.age);
                                born = (EditText) findViewById(R.id.born);
                                city = (EditText) findViewById(R.id.city);

                                age.setHint(itemList.get(2));
                                born.setHint(itemList.get(1));
                                city.setHint(itemList.get(0));
                            }

                            Button confirm = (Button) findViewById(R.id.confirm);
                            assert confirm != null;
                            confirm.setOnClickListener(
                                    new Button.OnClickListener() {
                                        public void onClick(View v) {
                                            AlertDialog.Builder alert = new AlertDialog.Builder(ItemFrame.this);
                                            alert.setTitle("Are yo sure?");

                                            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int whichButton) {
                                                    if (extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
                                                        if (writer.getText().toString().matches("") == false) {
                                                            itemList.set(0, "Writer: " + writer.getText().toString());
                                                        }
                                                        if (country.getText().toString().matches("") == false) {
                                                            itemList.set(1, "Country: " + country.getText().toString());
                                                        }
                                                        if (length.getText().toString().matches("") == false) {
                                                            itemList.set(2, "Length: " + length.getText().toString());
                                                        }
                                                        if (language.getText().toString().matches("") == false) {
                                                            itemList.set(3, "Language: " + language.getText().toString());
                                                        }
                                                        if (extras.getString("data").equals("Series")) {
                                                            if (episodes.getText().toString().matches("") == false) {
                                                                itemList.set(4, "Episodes: " + episodes.getText().toString().trim());
                                                            }
                                                        }
                                                    } else {
                                                        if (age.getText().toString().matches("") == false) {
                                                            itemList.set(2, "Age: " + age.getText().toString().trim());
                                                        }
                                                        if (born.getText().toString().matches("") == false) {
                                                            itemList.set(1, "Born: " + born.getText().toString());
                                                        }
                                                        if (age.getText().toString().matches("") == false) {
                                                            itemList.set(0, "City: " + city.getText().toString());
                                                        }
                                                    }

                                                    new ProgressTaskUpdate().execute();
                                                }
                                            });

                                            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int whichButton) {

                                                }
                                            });

                                            alert.show();
                                        }
                                    }
                            );
                        }
                    }
            );

            final AlertDialog.Builder builder = new AlertDialog.Builder(ItemFrame.this);
            builder.setTitle("Are you sure?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    new ProgressTaskDelete().execute();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            Button delete = (Button) findViewById(R.id.delete);
            assert delete != null;
            delete.setOnClickListener(
                    new Button.OnClickListener() {
                        public void onClick(View v) {
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    }
            );

        }
    }

    public class ProgressTaskUpdate extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute(){
            ProgressDialog = new ProgressDialog(ItemFrame.this);
            ProgressDialog.setIndeterminate(false);
            ProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            ProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            synchronized (this) {

                queue = Volley.newRequestQueue(ItemFrame.this);
                loaded = false;
                final Bundle extras = getIntent().getExtras();

                String url = "https://api.backendless.com/v1/data/" + extras.getString("data") + "/" + extras.getString("chosenID");

                JSONObject obj = new JSONObject();
                try {
                    if(extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
                        obj.put("Writer", itemList.get(0).toString().substring(8, itemList.get(0).toString().length()));
                        obj.put("Country", itemList.get(1).toString().substring(9, itemList.get(1).toString().length()));
                        obj.put("Length", itemList.get(2).toString().substring(8, itemList.get(2).toString().length()));
                        obj.put("Language", itemList.get(3).toString().substring(10, itemList.get(3).toString().length()));
                        if(extras.getString("data").equals("Series")) {
                            obj.put("Episodes", itemList.get(4).toString().substring(10, itemList.get(4).toString().length()));
                        }
                    } else {
                        obj.put("Age", itemList.get(2).toString().substring(5, itemList.get(2).toString().length()));
                        obj.put("Born", itemList.get(1).toString().substring(6, itemList.get(1).toString().length()));
                        obj.put("City", itemList.get(0).toString().substring(6, itemList.get(0).toString().length()));
                    }
                    if(imgUpdated == true) {
                        obj.put("Picture", imgURL);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println(obj);

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, obj, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        loaded = true;
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                        headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                        headers.put("Content-Type", "application/json");
                        headers.put("application-type", " REST");
                        return headers;
                    }
                };

                queue.add(request);

                //ak pridlho caka, chyba so spojenim
                final AlertDialog.Builder builder = new AlertDialog.Builder(ItemFrame.this);
                builder.setTitle("No connection");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(ItemFrame.this, MainFrame.class));
                    }
                });

                builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                int missed = 0;
                while(loaded != true) {
                    try {
                        this.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(missed == 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                        connected = false;
                        break;
                    } else missed++;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ProgressDialog.dismiss();
            if(connected == false) {
                return;
            }

            final Bundle extras = getIntent().getExtras();

            if(extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
                Intent i = new Intent(ItemFrame.this, TableFrame.class);
                i.putExtra("data", extras.getString("data"));
                startActivity(i);
            } else {
                Intent i = new Intent(ItemFrame.this, PeopleTableFrame.class);
                i.putExtra("data", "People");
                startActivity(i);
            }
        }
    }


    public class ProgressTaskDelete extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute(){
            ProgressDialog = new ProgressDialog(ItemFrame.this);
            ProgressDialog.setIndeterminate(false);
            ProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            ProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            synchronized (this) {

                queue = Volley.newRequestQueue(ItemFrame.this);
                loaded = false;
                final Bundle extras = getIntent().getExtras();

                String url = "https://api.backendless.com/v1/data/" + extras.getString("data") + "/" + extras.getString("chosenID");

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        loaded = true;
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        headers.put("application-id", "8D334F51-3DD1-3300-FFD0-266FF245E700");
                        headers.put("secret-key", "88341E4E-F12C-E1ED-FFFA-425CE2B92500");
                        //headers.put("Content-Type", "application/json");
                        headers.put("application-type", " REST");
                        return headers;
                    }
                };

                queue.add(request);

                //ak pridlho caka, chyba so spojenim
                final AlertDialog.Builder builder = new AlertDialog.Builder(ItemFrame.this);
                builder.setTitle("No connection");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(ItemFrame.this, MainFrame.class));
                    }
                });

                builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                });

                int missed = 0;
                while(loaded != true) {
                    try {
                        this.wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(missed == 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        });
                        connected = false;
                        break;
                    } else missed++;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ProgressDialog.dismiss();
            if(connected == false) {
                return;
            }

            final Bundle extras = getIntent().getExtras();

            if(extras.getString("data").equals("Movies") || extras.getString("data").equals("Series")) {
                Intent i = new Intent(ItemFrame.this, TableFrame.class);
                i.putExtra("data", extras.getString("data"));
                startActivity(i);
            } else {
                Intent i = new Intent(ItemFrame.this, PeopleTableFrame.class);
                i.putExtra("data", "People");
                startActivity(i);
            }
        }
    }




    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected void onPreExecute(){
            spinner = (ProgressBar) findViewById(R.id.spinner);
            spinner.setVisibility(View.VISIBLE);
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            spinner = (ProgressBar) findViewById(R.id.spinner);
            spinner.setVisibility(View.GONE);
            bmImage.setImageBitmap(result);
            img = result;
        }
    }
}